#!/bin/bash
apt install apt-transport-https
#TODO: Domain
#TODO: public IP FQDN Hostname
#ip a | grep inet | grep -Ev "^[\t ]+inet( (127|10)\\.|6 (fe|::1))"
#hostname --all-ip-addresses
#cat /etc/hosts | grep $(hostname)
curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
apt update
echo 'jitsi-videobridge jitsi-videobridge/jvb-hostname string meet.do.t13.one' | debconf-set-selections
echo 'jitsi-meet jitsi-meet/cert-choice select Generate a new self-signed certificate (You will later get a chance to obtain a Let's encrypt certificate)' | debconf-set-selections #TODO: Does that work?
export DEBIAN_FRONTEND=noninteractive
apt -y install jitsi-meet
echo "devnull@example.com" | /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
