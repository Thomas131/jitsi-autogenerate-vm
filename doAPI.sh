set -euxo pipefail
source CONFIG.sh
curl -X POST "https://api.digitalocean.com/v2/droplets" \
      -d'{ "name":"'"$DOMAIN"'", "region":"fra1", "size":"s-1vcpu-1gb", "image":"debian-10-x64", "user_data": "'"$(cat install.sh)"'", "ssh_keys":[ "'"$SSHKEY"'" ], "ipv6":true, "monitoring": true }' \
      -H "Authorization: Bearer $TOKEN" \
      -H "Content-Type: application/json"
